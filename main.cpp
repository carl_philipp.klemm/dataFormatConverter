#include <iostream>
#include <fstream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <filesystem>
#include <vector>
#include <string>
#include <assert.h>

#include "tokenize.h"

constexpr char classFilename[] = "classes.txt";

void printUsage(char* name)
{
	std::cout<<"Usage: "<<name<<" [INPUT DIRECTORY] [OUTPUT DIRECTORY] [OPTIONAL EXT]\n";
	exit(0);
}

std::vector<std::string> loadClasses(const std::filesystem::path& inDirPath)
{
	std::fstream classFile(inDirPath/std::filesystem::path(classFilename), std::ios_base::in);
	if(!classFile.is_open())
	{
		std::cout<<inDirPath/std::filesystem::path(classFilename)<<" dose not exist but is required\n";
		exit(1);

	}

	std::string className;
	std::vector<std::string> classes;
	while(!classFile.eof() && classFile.good())
	{
		std::getline(classFile, className);
		if(!className.empty())
			classes.push_back(className);
	}

	return classes;
}

std::vector< std::pair<cv::Rect, int> > yoloLableFileToRects(std::filesystem::path fileName, int cols, int rows)
{
	std::fstream lableFile(fileName);
	assert(lableFile.is_open());

	std::vector< std::pair<cv::Rect, int> > rects;

	while(!lableFile.eof() && lableFile.good())
	{
		std::string line;
		std::getline(lableFile, line);
		std::vector<std::string> tokens = tokenize(line, ' ');

		if(tokens.size() < 2)
			continue;
		if(tokens.size() != 5)
		{
			std::cout<<"Warning: \""<<line<<"\" is not a valid yolo lable\n";
			continue;
		}
		try
		{
			int centerX = std::stof(tokens[1])*cols;
			int centerY = std::stof(tokens[2])*rows;
			int width = std::stof(tokens[3])*cols;
			int height = std::stof(tokens[4])*rows;
			cv::Rect rect(centerX-width/2, centerY-height/2, width, height);
			int classNum = std::stoi(tokens[0]);
			rects.push_back(std::pair<cv::Rect, int>(rect, classNum));
			std::cout<<"got rect "<<rect<<" in image of size "<<cols<<'x'<<rows<<'\n';
		}
		catch(const std::invalid_argument& ex)
		{
			std::cout<<"Warning: \""<<line<<"\" is not a valid yolo lable\n";
			continue;
		}
	}

	return rects;
}

int main(int argc, char** argv)
{
	if(argc < 3)
		printUsage(argv[0]);

	std::filesystem::path inDirPath(argv[1]);
	std::filesystem::path outDirPath(argv[2]);

	std::string imageExtention(argc >= 4 ? argv[3] : ".png");

	std::filesystem::create_directory(outDirPath);
	if(!std::filesystem::is_directory(outDirPath))
	{
		std::cerr<<"unable to create output directory at "<<outDirPath<<'\n';
		return 1;
	}

	std::vector<std::string> classes = loadClasses(inDirPath);
	std::cout<<classes.size()<<" Classes: \n";
	for(const std::string& str : classes)
	{
		std::cout<<str<<'\n';
		std::filesystem::create_directory(outDirPath/str);
		if(!std::filesystem::is_directory(outDirPath/str))
		{
			std::cerr<<"unable to create directory "<<outDirPath/str<<'\n';
			return 1;
		}
	}

	std::filesystem::directory_iterator inDirIter(inDirPath);

	for(const std::filesystem::directory_entry& entry : inDirIter)
	{
		if(!entry.is_regular_file() && !entry.is_symlink())
			continue;
		if(entry.path().extension() != ".txt")
			continue;
		if(entry.path().filename().string().find(classFilename) != std::string::npos)
			continue;

		std::string imagePath = (entry.path().parent_path()/entry.path().stem()).string() + imageExtention;
		cv::Mat image = cv::imread(imagePath);
		if(!image.data)
		{
			std::cout<<"Warning: unable to load image from "<<imagePath<<'\n';
			continue;
		}

		std::vector< std::pair<cv::Rect, int> > rects = yoloLableFileToRects(entry, image.cols, image.rows);

		for(size_t i = 0; i < rects.size(); ++i)
		{
			const std::pair<cv::Rect, int>& rect = rects[i];
			if(rect.second > static_cast<int>(classes.size()-1) || rect.second < 0)
			{
				std::cout<<"Warning: "<<entry.path().filename()<<
					" contains a label with a class thas out of range for the classes in "<<classFilename<<'\n';
				continue;
			}

			cv::Mat crop(image, rect.first);
			std::string outPath = outDirPath/classes[rect.second]/(entry.path().stem().string() + "_" + std::to_string(i) + imageExtention);
			if(!cv::imwrite(outPath, crop))
			{
				std::cout<<"Warning: could not write image to "<<outPath<<'\n';
				continue;
			}
		}
	}

	return 0;
}
